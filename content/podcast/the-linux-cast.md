---
title: "The Linux Cast"
type: post
date: 2023-01-15T22:12:19-05:00
url: /the-linux-cast/
image: /images/2022-thumbs/the-linux-cast-logo.png
categories:
  - Podcast
tags:
  - podcast
  - blog
draft: false
---
## I Joined The Linux Cast
I'm proud to annoucne that I've been invited to be a host on [The Linux Cast](https://youtube.com/c/thelinuxcast) with Matt, Tyler, and Another guy I only being named DarkXero.  
The community there might know me as the "Gentoo Guy" but in truth I think of myself as a distro maintainers worst nightmare. *The end user who's willing to tinker.*
## What's Going to Happen
We'll see how chaotic the show comes to be however as it's doubling the number of hosts. Hopefully it just doesn't become yet another debate on XeroLinux like my experiences on [DistroTube's](https://youtube.com/c/distrotube) Patron Chats have been. Not that i mind of course.
Either way, I hope my insights can lead myself, my fellow hosts, and the community grow and get even better at contibuting and using our preferred operating system. And, maybe, just maybe I can talk them into switching to Gentoo.   

*Just Kidding*  

Honestly, I don't even use Gentoo right now, though I'll be honest for some reason I actually do kinda just want to go back to it. But that's another post for another time.

## What About DistroHacking
I might still do the DistroHacking streams. I just need to come with a time to do them. I'm eye balling 7pm EST on Fridays right now, but that's tentative. And honestly we need to do something other than review a distro, or install Gentoo. Some people call the show a podcast, I personally don't. I just call it a show, and honestly unless your joining us live. Your probably not having fun.  
I might need to figure out a format everyone can join in on. To be honest the show never really has been particularly watched.

## What happenned to Odyssee
I honestly just dont enjoy the platform. It's full of far right "Trumpeteers" conspiracy extremist, and hypocritical "free speech" evangelist. Then the company seems to promote the worst channels I could think they could promote. Then of course the SEC judgement against them, and the fact that they would sell LBC toy you rather than just let you mine it.  
Honestly, the more I learned about the platform, the less I liked it. Instead I joined a [PeerTube](https://videos.danksquad.org/c/joshua_lee) instance and that's going to be my alternate platform of choice. Since in all honesty, YouTube has grown to be too large and I don't see it existing as a platform in the next ten years. Then again i could be wrong.

## My Content Future
This years plans:
I'm partaking in the [100 Days of Homelab](https://100daysofhomelab.com/) challenge. Now whether or not I can go 100 days of messing around with things is up for debate. But, I might come up with something. Either way Im going to be making videos on it, assuming I actually remember to make videos. I also plan to post here at least more often, right now I have no set schedule, with my progress thfough the challenged and some fo the things I run into throughout the day.  
My goal with all this is to actually help someone figure something out, I'm not really chasing a career but if a career opportunity comes out of it, I at least want to show I earned it. As a general reminder, my content will always be available. Even if I have to setup a video platform myself.

### Connect With Me
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
