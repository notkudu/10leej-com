---
title: "Distrohacking: Is NixOS The Future?"
type: post
date: 2022-03-10T12:50:56-05:00
url: /s1e1/
image: /images/2022-thumbs/s1e1.png
categories:
  - Podcast
tags:
  - NixOS
  - Emacs
  - Slackware
draft: false
---
## Video
{{< youtube id="YRBPJJDAQ0o" >}}

## Shownotes
NixOS is a distro after my own heart, we cover the instalation and how cool I think it is, and yet somehow I got convinced by a guest known as Dem to try slackware, while I also goof around with Emacs a bit. You'll find while the episode is long, we almost always have a good time.

### Links:
[NixOS](https://nixos.org/)  
[Slackware](http://www.slackware.com/)  
[GNU Emacs](https://www.gnu.org/software/emacs/)  

## Subscribe
[Audio](https://10leej.com/feed.xml)  
[YouTube](https://www.youtube.com/channel/UCNvl_86ygZXRuXjxbONI5jA)

### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)  
*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
