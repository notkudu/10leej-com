---
title: "We're Now Open to Community Posts"
type: post
date: 2022-09-12T04:34:23-04:00
url: /open-to-community-posts/
image: /images/2022-thumbs/open-to-community-posts.png
categories:
  - Blog
  - News
tags:
  - Meta
draft: false
---
## We're Open!
Yep thats right we're now open.  
Basically, I decided we're open to community posts only just late last night. i got it listed out in the README.md file ion the [git repository](https://gitlab.com/10leej/10leej-com) but there are a few rules.

## Rules
1. We are NERDS not politicians. I'll welcome a healthy debate, but lets not bring debates to a toxic level.
2. Your post is subject to moderation. If your post brings a lot of controversy it will be deleted.
3. Dont advocate piracy or share illegal content. We already know how to download a torrent, no need to tell us how.
4. Dont use affiliate links without the approval of this repositories maintainer which is [currently Joshua Lee aka 10leej](https://gitlab.com/10leej)
5. To have a post removed after it's been merged, open a merge request removing it and the reasons for doing so.
6. Understand the licensing of this website is [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) as such your post must comply with that licensing.

## How to Post
This website was made using Hugo, there's really no magical scripting here. You just git clone the website repo, make sure the [Mainroad theme](https://github.com/Vimux/Mainroad) is installed.  
I myself use the vscode [Hugo Helper](https://marketplace.visualstudio.com/items?itemName=rusnasonov.vscode-hugo) tool, but am working on looking into this fancy new text editor I haven't announced I'm switching too yet *(it's written in lua btw)*.

But once you get the fancy bit pulled in, just make a new post like this:

````
hugo new blog/articlename.md
````

This will create a file in ***content/blog*** called *articlename.md* from there it'll pull in my draft template from ***archetypes/default.md*** just modify accordingly.  

To see a formatted render use hugo to run a test site
````
hugo server
````
## The Future

So I'm debating on the future of this blog, I think it's gonna stick around, but I do want to mess around with the theme's colors a bit and give it a nice dark theme.  
As for the content, I'm gonna really start posting articles on what I'm working on as I go through the [100 Days of Homelab](https://100daysofhomelab.com/) or something like that. I'm not sure I can really commit to the challenge. But, I do plan on working on a [LPIC-1](https://www.lpi.org/our-certifications/lpic-1-overview) or [RHCSA](https://www.redhat.com/en/services/certification/rhcsa) cert. We'll find out. Either way, I am going to be posting here, or promising to post more often here.  

> Don't expect a schedule.

## Connect With Me
### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.