---
title: "12-25-2022 Planar Ally Update"
type: post
date: 2023-01-15T22:48:42-05:00
url: /12-25-2022-planar-ally-update/
image: /images/2022-thumbs/12-25-2022-planar-ally-update.png
categories:
  - News
tags:
  - planarally
draft: false
---

I update Planar Ally to verison 2022.3, you can read the release notes over [here](https://www.planarally.io/blog/release-2022.3/) Kruptein definately put a lot of fantastic work into this release. I did notice a couple of issues of which i did file bug reports for. Reference [#1143](https://github.com/Kruptein/PlanarAlly/issues/1143) and [#1144](https://github.com/Kruptein/PlanarAlly/issues/1144).  
As always please if you have an issue please reference the PlanarAlly [issue list](https://github.com/Kruptein/PlanarAlly/issues) or check out the PlanarAlly [discord](https://discord.gg/mubGnTe), we're a small community still.

As for 2022, we had two instances of downtime, both of which are my fault with stopping the container once for an update and forgetting to turn it back on, and another time when the disk filled on the VPS Im using to host this website and the container. I probably still hit the 99% uptime goal most sys admins would ask for not that it's something I chase for myself.
That said, I do plan a redeployment of the VPS to basically start me out with a fresh template. The current Debian instalation has persisted since April of 2020 with lot of tweaks and changes I honestly don't have a record of, and while thats great. I've taken to playing around with RHEL based systems and gained quite a bit of experience with them and honestly I think I might just migrate off Debian to a system Im currently familiar with.

### Connect With Me
#### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
