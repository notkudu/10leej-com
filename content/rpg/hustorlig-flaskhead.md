---
title: "Hustorlig Flaskhead"
type: post
date: 2021-11-14T00:05:10-05:00
url: /hustorlig-flaskhead/
image: /images/2020-thumbs/hustorlig-flaskhead.jpg
categories:
  - RPGs
tags:
  - Pathfinder
---
# Hustorlig "Hus" Flaskhead of Clan Honorbringer

## Family
### Father - Bhardor
Captain of the Gravenport City Watch. Bhardor is the well respected leader of **Clan Honorbringer**
 
### Mother - Mildrid
Stay at home Mom, Mildred is a caring woman.

### Elder Brother - Oskarr
A captain of the city watch, Oskar is a strong leader and heir to the clan's leadership.

### Elder Sister - Matilda  
A cleric in service of *Iridia* the dwarven diety of knowledge. Matilda has shown the capabilities to be an Oracle, a rarity among dwarves.

### Twin Sister - Beatrice  
A champion in service of the King's army. Beatrice is Hustorlig's twin sister and they share a strong bond as siblings.

### Younger Brother - Grimlick
Barely into adulthood Grimlick is a bit hotheaded even for a dwarf, but he's proven to be a reliable ally.
 
## Affiliations
### Gravenport City Watch
 - Self explanatory
### Clan Honorbringer
 - A large dwarven clan known for it's long service in the city of Gravenport, tracing it's roots all the way to the founding of the city itself, with the name Honorbringer being on the city's Charter as proof of the Clan's bond with the city. As a noble family of the city, Clan Honorbringer demands a high degree of dwarven honor and respect among it's members.

### The Gentlemen Bastards
**Cale Lindon**  
A human cleric and long time childhood friend, his family were well regarded in the local temples.  

**Aias Carlen**  
A Half-Elven rogue who was born into a family of merchants.  

Originally a group of youths who vandalised property of the wealthier members of Gravenport's upper class. They eventually elected to be a group of bounty hunters with a small bit of notoriety in precision tactics.

### Maere Tagachi
 - A blue changling, seems like he's good friends with Quincy. Don't know too much abouit him. Seems like a powerful fighter of some kind and fairly quick to pick a fight. **Hus** might need to keep an eye on this one.
### Matilda
 - His elder sister who had a vision of Hustorlig as a famed hero who fights a great evil though whether he succeeds in defeating it or not is unclear, however she says his potential in nearly limitless.
### Quincy
 - A gnome **Hus** encountered near Lake Oakshade, **Hus** helped the gnome escape a dangerous encounter with a giant. He seems very confidant in his abilities though his moral judgement seems a bit lacking.
### Snag  
 - A goblin he met while acting on a job with a group of fellow bounty hunters hunting goblin raiders. **Hus** and Snag respect each other's abilities, but he wishes Snag could probably pratice a bit of patience.

## Enemies/Anatagonizers

### Strelin Moltengleam
A dwarven wizard that had his spellbook burned by **Hus** on accident, the wizard 

### Dwarven Clans
**Clan Ordo**  
- Trimmed Clan Leader *Cander*'s beard too short whole he slept.  

**Clan Thunderbrew**  
- Stole the clans mead recipe and sold it to *Clan Ironale*  

### Family
**Bhardor**  
 - **Hus** betrayed the family tradition of service to be a bounty hunter. Causing his father to all but disown Hus from the Clan.  

### Bounties
**Ermglen Greenfellow**  
 - A human wizard apprentice of which Hustorlig apprehended and accidently burned his master's stolen spellbook.  


## Backstory
Hustorlig Flaskhead was born into *Clan Honorbringer* which is well know know for its generations of service in the City Watch with roots tracing back to the very founding of the city of Gravenport itself. Hustorlig was trained as a soldier from the day he could swing a weapon. Showing promise in his skills as a fighter his father **Bhardor** instilled a strong sense of honor into Hustorlig, though the tradition never really seemed to sink into **Hus**.  

Causing some troubles in the city of Gravenport among it's upper class **Hus** and his friends Cale and Aias called themselves the *Gentlemen Bastards* as a joke to their somewhat noble heritages. Never really doing anything serious they were known for some minor bits of vandalism and pranks.  

One day his sister **Matilda** collapsed while in prayer with **Hus** being the first to her aid, she claimed to have had a vision. Though she wasn't, and still isn't, able to clearly tell what the vision is. Her only assumption is that it's a divine prophecy of some kind. This caused **Hus** a great deal of concern since he never really saw himself as a hero. But ultimately, **Hus** decided that if that vision was to be real he would need to be able to ensure that if he were to fight such an enemy, he would at least give himself a chance of victory.

Forgoing his families tradition of service in the city watch or even the kingdom's military, **Hus** elected to live with a bit more freedom in his life and elected to be a bounty hunter. A rather rough profession, but it paid better than being a watchman or soldier. This angered his father greatly who saw it as a betrayal of *Clan Honorbringer's* honor, which ultimately ended up with Hustorlig being all but disowned from the clan. Forgoing the name Honorbringer took on the name Flaskhead, and rejoined his friends from the *Gentlemen Bastards* as a team of bounty hunters.  

Taking on a bounty to take in the apprentice wizard **Ermglen Greenfellow** for the charges of misuse of magic in public spaces, and destruction of public property (100g bounty), **Hus** and his team managed to capture the young wizard. But in doing so knocked the stand the Ermglen's master's spellbook, being unable to pay for a replacement **Hus** and his team elected to do personal contracts for the powerful dwarven wizard **Strelin Moltengleam** the first few were minor with **Hus** and his friends leaving the city for collecting spellcasting materials for the wizard but his last contract the wizard sent the *Gentlemen Bastards* into a goblin raider camp to clear them out and collect blood samples for a spell he was working on.  

His friend **Cale** refused to do the task leaving just **Hus** and **Aias** to perform the grim task. Hiring a mercenary named **Gruts** to join them they infiltrated the camp killing every goblin warrior who dared stand up to them, however **Hus** and **Aias** refused to attack the goblin prisoner **Snag** who had no way of defending herself. **Hus** stood in **Gruts** way and eventually fought him alongside Aias. It was a tough fight which ended up with **Aias** taking a fatal wound, but **Hus** freed **Snag** who used a timely bomb to defeat **Gruts**


















