---
title: "Sponsors"
type: post
date: 2023-02-12T02:30:19-05:00
url: /sponsors
categories:
  - News
tags:
  - sponsors
draft: false
toc: false
---

## Sponsors
Currently None!

## Coorperate Sponsorship Guidelines
1. I reserve the right of opinion, this means if your product is bad. Then I will tell not only you it's bad. But my audience as well.  
2. I only accept payments through United States Dollars, I will not accept any payments in cryptocurrncies ior in any foreign denomination. Nor will I accept payments in viewership, subscription, comments.
3. I will only promote a giveaway that aligns with the various laws and regulations in the continental US states.
4. If your product is a game, I will not "collaberate" with you. You'll be considered a sponsor unless negotiated otherwise.
5. If your product is software, it must ship a binary compatible wth GNU/Linux since that is the operating system I choose to use.
6. All payments are considered final with no refund, nor any expectation of such.

## Community Sponsorship Guidelines
1. [Librepay](https://liberapay.com/10leej/)/Patreon (currently doesn't exist)/[Paypal](https://www.paypal.com/paypalme/10leej) are the official sources of sending me currency, I do not accept cryptocurrency, money orders, checks, credit card numbers, bank transfers.

2. Physical Products can be sent to me via my [Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share) anything received via this method is considered under the community sponsorship guidelnes.

### Connect With Me
#### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
