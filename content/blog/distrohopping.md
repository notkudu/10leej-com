---
title: "Distro Hopping is Not Stupid"
type: post
date: 2022-09-11T20:42:39-04:00
url: /distrohopping.md/
image: /images/2022-thumbs/distrohopping.png
categories:
  - Blog
tags:
  - Linux
draft: true
---

## It's not dumb
In my opinion when you decide it's time to change your Linux Distribution there are two things
### 1. Your Curious
Your new to linux, or you want to see how something is done.
### 2. Your Bored and Want to Try Something New.
You've used the same distro forever, and while it's worked for you, you just decide that maybe... Just maybe. That brand new distro you haven't looked at yet might do something you never considered looking at before.
### 3. You NEED to make the change.
Your profession demands you use a specific set of tools.

## 

## Connect With Me
### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.