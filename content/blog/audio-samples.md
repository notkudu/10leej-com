---
title: "Audio Samples"
type: post
date: 2022-02-27T17:50:50-05:00
url: /audio-samples/
image: /images/2020-thumbs/audio-samples.jpg
categories:
  - Blog
tags:
  - Audio
draft: false
---

# So I've been working on some audio stuff

Here's some audio smaples for my current setup.

Linux Mint 20.3
PulseAudio
Cinnamon Desktop

Audio Technica AT2020
Alesis Multimix 4USBFX (I honestly hate this thing but it was cheap enough xlr at the time)

[nevoyu-sample.tar.gz](/files/nevoyu-sample.tar.gz)

Thats literally as raw a recording that a recording can be, I included the audacity project file too for proofs. but this mixer has no effects and the tuning is flat on the 12kHz and 80Hz and it feeds into the PC via the mixer's usb port.

I've been using a se v7 mic for about two years now this one is still new to me so I really haven't gotten around with playing too much other than setting levels and gain. I have done any EQ, dont have compression, nor even a noise gate.
