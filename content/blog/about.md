---
title: "About"
type: post
date: 2023-02-12T03:02:52-05:00
url: /about
categories:
  - News
tags:
  - about
draft: false
toc: false
---

## Hello, I'm Joshua Lee.

Hi there, I'm Joshua Lee, a near-middle-aged white guy who lives in a small town in Northwestern Ohio. You likely found me through my [10leej Youtube](https://youtube.com/@10leej) channel, but you'll also find my [Vlog Channel](https://youtube.com/@joshua_lee732), which I don't really advertise.

Professionally, I work in [Plastic Injection Molding](https://en.wikipedia.org/wiki/Injection_moulding), and unfortunately, I know a bit too much about how to squirt molten plastic into a steel sandwich. However, I am a hobbyist Linux user, a Tabletop RPG fanatic, and a bit of an advocate.

I'm not really religious; instead, I follow a few basic tenets centering around Faith, Respect, and Pride. If you need context on that, I spoke about it in [this article](https://10leej.com/the-meaning-of-life-and-everything-is-42). It has evolved since I wrote that, but it's still pretty straightforward on what I believe in. This philosophy that I follow has led me to different careers, friends, adventures, and overall, a fantastic set of experiences so far.

## Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](mailto:josh@10leej.com)

## Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to respect my freedom of speech and thoughts about their products. If your product is subpar, I will, in fact, express my honest opinion beforehand. All donations are non-refundable.
