---
title: "I Bought a Pinebook Pro"
type: post
date: 2022-08-09T01:35:56-04:00
url: /i-bought-a-pinebook-pro/
image: /images/pine64-logo-text-standard-black-blue.svg
categories:
  - Blog
tags:
  - Pinebook
  - Pine64
  - Manjaro
draft: true
---

## Introduction

It's been a good while since I was so excited about a product, and honestly the first thing I did after updating Manjaro-ARM was install everything I need to make this very post. Which of course is giving me time to aquaint myself with  the keyboard, which feels.. borderline between ok and good. Mor eon that later.  
First off, I'm going to attempt to daily drive this thing, 30 days. If I can go that far I think I'm golden.That said this is still very much early impressions here.

## First Impressions

First off, the keyboard is good, I dont immediately hate it. There's not a lot of chassis flex, which is probably what makes it so good. The trackpad however seems to have a fairly lot polling rate which can cause issues specifically when gitting some buttons on the kde plasma desktop that ships with the pinebook. And of course I had to add a border size to the windows for mucht he same reason. The speakers make sound and work, wifi supports my 5GHz networkjust fine. The packaging is about what I expected (just a cardboard box in a cardboard box).  
The battery did come with a charge, which is nice to see, there's a fiewall installed (but not enabled) and the Manjaro ARM installer did not ask if I wanted to setup disk encryption (pretty big deal to me since this is a laptop).
Oh and the speakers sound better than the ones on my Lemur Pro, and I don't really like how the deleted key is held under a modifier key beneather the backspace. I'd prefeet that to be seperate.

## Performance

It's... serviceable. Youtube managed 720p at 60 fps plenty smooth enough, firefox loads reasonably quick (faster than the snap on my Raid 0 3 nvme disk setup met with a 12gen i5 desktop). I haven't given it the gentoo test yet (and probably won't either). The fact I can playback youtube is fine for me, I'm not gonna really play games on my laptop when I have a firebrather gaming workstation PC in the same room, and I'm not really out of the house enough to justify the idea of "portable gaming"
I did install quaternion from teh AUR using yay, which did take about 7 minutes (I really didn't time it). Gonna work on a few more things still and we'll see how this works out.

## Battery Life

So the device shipped with about 80% of it's battery and giving it time to run itself down I've seen a 2% drop in about an hours use giving the reported 8 hours of remaining time a believable status. Afterall I'm rendering my hugo test page with firefox and writing this article in vim. So it's a reasonable estimate. Honestly I think about 6 hours is a good solid guess and really what I think would be the minimum for a laptop.

## Quality Control Impressions

- The speakers do pop and crackle a bit, and once I quit audio playback they seem to make a popping sound a bit later. Honestly I think this is software related. Manjaro ARM is using pipewire by default.
- The trackpad shipped with he bottom left corner raise on it, alost like it was installed incorrectly, Definately gonna tear it apart and see if i can fix it.
- The power plug what shipped with the pinebook has a broken clip on the US outlet adapter.

## Conclusion

So overall I was happy with the Pinebook Pro, then an update from Manjaro ARM brocked the DHCPcd stack in the linux kernel, this prompted me to compile my own kernel (which did fix it). But here's the issue. Manjaro ARM's KDE spin is the out of the box experience **THIS SHOULD NOT HAPPEN!** on what's supposed to be the official flavor of Linux then this should not happen **EVER**.  
And, then I read [this article](https://blog.brixit.nl/why-i-left-pine64/) and I almost regret my purchase entirely. I'm really not sure why I want to continue to support this company.

## Connect With Me
### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
