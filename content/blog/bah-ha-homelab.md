---
title: "Bah Ha Homelab"
type: post
date: 2022-09-13T05:34:02-04:00
url: /bah-ha-homelab/
image: /images/2022-thumbs/bah-ha-homelab.png
categories:
  - Blog
tags:
  - Redhat
draft: true
---
{{< youtube id="w7Ft2ymGmfc" >}}

## The Homelab

### Connect With Me
#### Social Media
[Blog](https://10leej.com)  
[Mastodon](https://mstdn.starnix.network/web/@10leej)  
[Discord](https://discord.com/invite/cUfbCBF)  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org)  
[Email](josh@10leej.com)

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.