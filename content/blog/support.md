---
title: "Support"
type: post
date: 2023-10-19T18:49:38-04:00
url: /support/
categories:
  - Blog
tags:
  - Support
draft: false 
---

## Pay with Cash
Send me cash! I've vowed several times I'll never put any content behind paywalls. Instead you're giving me money because you like me. That is a better driver to me to make awesome content rather than me try to sell you a new product.

* [LiberaPay](https://liberapay.com/10leej/) --recurring subscriptions  
* [PayPal](https://www.paypal.com/paypalme/10leej) --one time payments
* [YouTube Memberships](https://www.youtube.com/channel/UCNvl_86ygZXRuXjxbONI5jA/join) --if you dont like the other platforms or just don't like LiberaPay.

## Pay With Stuff
You can send me stuff based on my wishlists

* [Amazon: Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share) --kinda a catch all  
* [Amazon: Books](https://www.amazon.com/hz/wishlist/ls/TY5DMFHWM919?ref_=wl_share) --I'm a reader
