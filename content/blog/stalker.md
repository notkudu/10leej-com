---
title: "How to Find Me"
type: post
date: 2022-08-08T09:46:41-04:00
url: /contact
categories:
  - News
tags:
  - stalker
draft: false
toc: false
---


## Links
[Blog](https://10leej.com): I shout at things here. In fact your already here, so why not stay?  
[YouTube](https://youtube.com/@10leej): I primarily post videos and streams here.  
[PeerTube](https://videos.danksquad.org/c/joshua_lee/videos?languageOneOf=en&s=2): I sync the YouTube channel to here, sometimes I might exclusive content here as well.  
[Mastodon](https://fosstodon.org/@10leej): I'm a nerd here. *- you can follow from [Mastodon](https://joinmastodon.org/) and any [ActivityPub](https://www.w3.org/TR/activitypub/) enabled instance*  
[Discord](https://discord.com/invite/cUfbCBF): I'm a pretty chill dude here.  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org): I make developers angry here. Direct is @10leej:matrix.org   
[X (formerly Twitter)](https://twitter.com/10leej): I get in political fights here.  
[reddit](https://www.reddit.com/r/10leej/): I don't even know what to do here.  

## Appearances
### The Linux Cast
I'm also a cohost on [The linux Cast](https://thelinuxcast.org/) a podcast where Matt, Tyler, Steve and I hangout and chat Linux.
### Tech Over Tea
I've made an appearance on [Tech Over Tea #168](https://youtu.be/Lo_OV2EC4-Y?si=ztvCYi6Aq7-8jfxG)
### XeroTalk
I've made an Appearance on [XeroTalk](https://youtu.be/ZRGRXk9yqxE?si=BAO5gmj1-5P9pQVd)
### Other
I'm also a fairly common guest in [DistroTube's Patron Chats](https://youtube.com/playlist?list=PL5--8gKSku16vljmOS51wV9MXajOmkQPH&si=dD_LOm-Y_3xpoJBU)
And you'll find me on various channels on YouTube and other media platforms. Most historically I'm also a mumble room attendee in [Linux Unplugged by Jupiter Broadcasting](https://linuxunplugged.com/)

## Support
[YouTube Memberships](https://www.youtube.com/channel/UCNvl_86ygZXRuXjxbONI5jA/join)  
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share): Buy me Gifts here. I really need that bookshelf.   
[Librepay](https://liberapay.com/10leej/): Or Just Send me money.

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my [sponsorship policy](https://10leej.com/stalker)

## Some video
{{< youtube id="SCJsahOAB94" >}}  
