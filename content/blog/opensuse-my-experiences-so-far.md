---
title: "openSUSE: My Experiences So Far"
type: post
date: 2021-08-31T07:42:37-04:00
url: /opensuse-my-experiences-so-far/
image: /images/opensuse-gecko.png
categories:
  - Blog
tags:
  - openSUSE
  - Linux
  - Distributions
  - Distro Reviews
draft: false
---
<!--more-->

![opensuse-gecko](/images/opensuse-gecko.png)

## I Had to Fight To Make This One Work
Not gonna lie, my introduction to openSUSE was a bit painful. I initially went the window manager route and while I had it working I just didn't like what I kept runnig into. I blamed YaST for many of my pains. Not to mention that I just didn't like the installation process behind it.  
The Installer, which I assume is YaST, was slow and felt unresponsive, and the installation process took FOREVER. Even using a DVD image with external repositories disabled it still took well over fifteen minutes for the system to install.  
I don't necessarily count this against the distro, but I imagine it could be faster, or at least it could be more transparent about what it's doing. I've yet to take the time to see if I can track down some source code to see if I can figure out why this is the case.  
Out of the box openSUE has to be the most frustrating distro I ever tried to get setup with. I quickly found the documentation lacking in few too many "I'm new to the SUSE experience" and I still struggle with navigating the clueterfuck that is the distro's website (debian's is worse though). I literally never discovered packman until I googled it. Which I guess isn't all that bad, but they could at least pt a "Getting started with SUSE" guide or something on the website.
There doesn't seem to be a real official torrent download, which made my life hell as I can at times have a spotty internet connection making the DVD iso's basically unusable (even if they pass the checksums) SO I'm limited to the network installers and McDonalds tethering a solid connection off my phone.  Of course  you can tell from the simple wget output that every download link hops through 5-6 opensuse servers to serve my http download. THis proved to be a Pain in My Ass.
Of course now that we got that cleared up.  

### openSUSE is simply amazing
Yep, that's right. I honestly think openSUSE is a great distro. It might not be noob friendly, or exactly poplar here in my corner of the internet. But it's one of those "Just Works" distro's that doesn't really bother to hold your hand. In fact with YaST it feels like the dev's actually WANT you to tinker with it. And break it.  
Yes, I personally have found openSUSE easier to break than ArchLinux from Gnome 40 crashing, to Plasma not respecting any settings I make in it.  
Down to XFCE4 becoming slow and sluggish.

And yet, they ahve this tool called "Snapper" and a file system called "btrfs" on root. Think on this. With Tumbleweed you have btrfs snapshots setup on a preconfigured bleeding edge rolling release distro. There's like the goal of nearly every ArchLinux fanboy. It's what I always try to accomplish with Gentoo. And here they've been doing this for years at this point.
Sure Zypper could be quicker, but I can rollback as far as my initial installation if I wanted to. Then with YaST you have an easy to reach GUI for... Almost everything, with not really too much left out (unless you REALLYreally want to rice your bootloader).
Overall, openSUSE has made my life.... easier in a way.
Oh and Leap is pretty cool. It's like Redhat levels of stable, has snapshots setup for the hell of it, and the packages are 1:1 with biggy daddy SUSE Enterprise, which is pretty amazing. 


### It leads me to wonder why openSUSE isn't exactly popular.  
I mean, unlike debian the distro doesn't seem to have fifty different people pulling it fifty different ways, or Ubuntu where the companies innovating on things, the community doesn't exactly agree with. It honestly seems to me that SUSE can't really much wrong. At leats on the surface.  
I admit that I haven't really looked at what's going on upstream in the project, but the community doesn't seem as passionnate as Fedora's, as popular as Arch's, or really even as knowledgeable and helpful as Gentoo's (seriously #gentoo on libera chat is simple amazing).  
I can submit questions and they do get answered yes, but everyone seems so... Profrssional about it? I guess. Quite to drastic change from #RTFM nation of Arch, or the Fedora communities "Maybe you should use Flatpak and not rpmfusion!"

### Conclusion
I'm not a developer, I'm not by any means a real power user. I'm just a guy who likes to tinker. And I might just have found a home that lets me do that. I think.
Now to see how long I can stick around here...
