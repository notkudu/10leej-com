---
title: "Solus Is Back"
type: post
date: 2023-04-16T14:30:25-04:00
url: /solus-is-back/
image: /images/2022-thumbs/solus-is-back.png
categories:
  - Blog
tags:
  - solus
  - linux
  -
draft: false
---

![solos-press-logo](/images/solus-is-back.png)

## The Announcement
Today I woke up to find [this reddit](https://old.reddit.com/r/SolusProject/comments/12ndrvt/righting_the_ship/) post from Josh Strobl, a developer I follow. This is a pretty big deal for meas Solus was my first distro experience where I honestly never felt that it was ever required for me to "use a terminal"
This was well before the days of flatpak and snaps (which Solus still doesn't have a graphical store for). The past six or so months have apparently really put a damper on the distribution with a [website outage](https://twitter.com/SolusProject/status/1622909056313597953), and then the package [build system went down](https://www.reddit.com/r/SolusProject/comments/125pbxz/comment/je7r417/?utm%255C_source=reddit&utm%255C_medium=web2x&context=3) which effectively means there's been no package updates for months now.
Which eventually resulted in content creators like [DistroTube](https://www.youtube.com/watch?v=U1kbOda7o3w), [The Linux Experiment](https://www.youtube.com/watch?v=9eVCn6FAsBA). Not to mention blog, most notable [BoilingSteam](https://boilingsteam.com/time-to-stop-using-and-recommending-solus/), and even the [Budgie Desktop Upstream](https://docs.buddiesofbudgie.org/user/getting-budgie) depreciating the once flagship distro for the project.

## Sailing the Ship
So, how excited am I for this? Well I installed it

![screenshot with neofetch while writing this article](/images/i-installed-solus.png)

Yep i installed Solus, I had to pull the Arc GPU out to accomplish it and hook up a USB dongle to get a working network connection to update the **ancient** [install image from July 2021](https://getsol.us/2021/07/11/solus-4-3-released/).
I then proceeded to join the IRC rooms and announcement my return, which then I'm pointed at the hot new [Matrix Space](https://matrix.to/#/#solus:matrix.org) they switched to.

## Giving Back and Helping Solus
I honestly don't want this distro to die, so I'm going to be commiting $25 per month to the [Solus OpenCollective](https://opencollective.com/getsolus) once I see their announcement on Tuesday. This should put me in a tier where I have access to early news announcements, weekly ISO snapshots, and early release builds for all versions. I'll hopefully be able to help out as I promised to revive an old effort of mine to get the [Calamares Installer](https://calamares.io/) working with eopkg, so hopefully we can enable an option for a network install, which installs any of the four releases and only publish a single image.
The big issue is we would have to teahc calamares how to use eopkg as it [doesn't quite support it yet](https://github.com/calamares/calamares/blob/calamares/src/modules/packages/packages.conf#L22)
[I announced my effort on the subreddit](https://old.reddit.com/r/SolusProject/comments/12ndrvt/righting_the_ship/jgiqorq/) so maybe, just maybe I might pickup a new skill. Of course at the time of this writing this is purely a third party effort as I'm a member of the Solus Team.

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)


