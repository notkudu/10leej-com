# Hello
This website is generated using [Hugo](https://gohugo.io/)  
The used theme is [Mainroad](https://github.com/Vimux/Mainroad)


## Build Instructions
To build a copy of the site you just install "hugo" on whatever platform you need.

````
git clone https://gitlab.com/10leej/10leej-com
````

change into the site directory

````
cd 10leej-com
````

then build the site

````
hugo
````

This will create a ***public/***  folder and there's a handy index.html.

## RSS

Hugo does support RSS all published content will be in the feed whether you like it or not.

````
https://10leej.com/index.xml
````

## Can I publish a post?
Yes you can! I will welcome any pull/merge request, There are no formatting guidelines at this time.

To make a post you need to tell hugo to make a post first

````
hugo new blog/articlename.md
````

This will create a file in ***content/blog*** called *articlename.md* from there it'll pull in my draft template from archetypes/default.md just modify accordingly.  

To see a formatted render use hugo to run a test site
````
hugo server
````

### Community Post Rules
1. We are NERDS not politicians. I'll welcome a healthy debate, but lets not bring debates to a toxic level.
2. Your post is subject to moderation. If your post brings a lot of controversy it will be deleted.
3. Dont advocate piracy or share illegal content. We already know how to download a torrent, no need to tell us how.
4. Dont use affiliate links without the approval of this repositories maintainer which is [currently Joshua Lee aka 10leej](https://gitlab.com/10leej)
5. To have a post removed after it's been merged, open a merge request removing it and the reasons for doing so.
6. Understand the licensing of this website is [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) as such your post must comply with that licensing.
