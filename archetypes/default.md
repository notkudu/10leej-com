---
title: "{{ replace .Name "-" " " | title }}"
type: post
date: {{ .Date }}
url: /{{ .Name }}/
image: /images/2022-thumbs/{{ .Name }}.png
categories:
  - Blog
  - News
tags:
  - Ubuntu
draft: true
---
{{< youtube id="w7Ft2ymGmfc" >}}

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.